adaptEnterErrorSurveyText
==============

Use your own string and text when user try to enter a survey : not started / expired / bad token.

## Installation

- This plugin need [renderMessage](https://gitlab.com/SondagesPro/coreAndTools/renderMessage)

### Via GIT
- Go to your LimeSurvey Directory (version up to 2.06 only)
- Clone in the plugins directory

```
git clone https://gitlab.com/SondagesPro/coreAndTools/adaptEnterErrorSurveyText
```

## Documentation
- See Plugins settings for set your messages.

## Home page & Copyright
- HomePage <http://extensions.sondages.pro/>
- Copyright © 2018 Denis Chenu <http://sondages.pro>
- Copyright © 2018 Dialogs <http://www.dialogs.ca/>

Distributed under [GNU GENERAL PUBLIC LICENSE Version 3](http://www.gnu.org/licenses/gpl.txt) licence
